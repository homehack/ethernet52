Wiznet W5200 Ethernet library
=============================

Based on Arduino IDE 1.0.1 Ethernet Library with additions:

1. New W5200.h and W5200.cpp files to replace original W5100.h and W5100.cpp
2. Code updated to use W5200 class instead of W5100 class.


